// eslint-disable-next-line
import React, {Component} from 'react'
import {times as _times} from 'lodash'
import styles from './styles.css'

class App extends Component {
  render() {
    const messages = _times(6, i => <div key={i}>Debug {i}</div>)
    return (
      <div className={styles.colors}>
        {messages}
      </div>
    )
  }
}

export default App
