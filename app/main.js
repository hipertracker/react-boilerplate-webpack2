// eslint-disable-next-line
import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery'
import App from './components/App'

const placeholder = 'app'

if (process.env.NODE_ENV === 'test') {
  $('body').append($(`<div id="${placeholder}"/>`))
}

ReactDOM.render(React.createElement(App), document.getElementById(placeholder))
