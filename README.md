# react-boilerplate-webpack2

Simple React boilerplate using Webpack2, with tree-shaking, Eslint, Karma unit tests and coverage with Instambul treesholds.

The settings are based on [kentcdodds/es6-todomvc](ttps://github.com/kentcdodds/es6-todomvc) and excellent ["Optimize React size and performance with Webpack production plugins"](https://egghead.io/lessons/tools-optimize-react-size-and-performance-with-webpack-production-plugins#/tab-code) (egghead)

##  Setup
npm run setup

## Development

npm start

## Testing

npm run watch:test

## Code coverage

cd coverage && http-server -p 3000

open http://localhost:3000

## Todo

* Add example from [Cerebral Boilerplate](https://github.com/cerebral/cerebral-boilerplate). One for (default) immutable [Baobab](https://github.com/Yomguithereal/baobab), and another one for mutable [tree-state](https://github.com/cerebral/state-tree).

* Add alternative [Brunch](http://brunch.io/) which is used by [Phoenix](http://www.phoenixframework.org/).

The best UI state libs (IMHO):

* [Cerebral](http://www.cerebraljs.com/)

* [Mobx](https://github.com/mobxjs/mobx)
